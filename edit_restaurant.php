<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//Initialize Session variable if it hasn't been done already.
	if(!isset($_SESSION))
	{
		session_start();
		
	}
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	
	//If the authentication and username variables have not be setup
	//kick the user back to the login page
	if( !($_SESSION['authenticated'] && !empty($_SESSION['username'])) )
	{
		echo "user is not authenticated";
		//header("Location: http://$host$uri/");
	}

		if(!empty($_GET['id']))
		{

			$m = new MongoClient();

			// select a database
			$db = $m->food305_db;
			
			$collection = $db->restaurant;
					
			$item = $collection->findOne(array('_id' => new MongoId($_GET['id'])));
			
			$id = $_GET['id'];
			$name = $item['name'];
			$location = $item['location'];
			$phone = $item['phone'];
		
		}
		
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Admin Restaurant Modify</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
  </head>
	<body>
		<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
		<form role="form" action="process_request.php" method="post">
			<div class="form-group">
				 
				<label for="RestNameInput">
					Restaurant Name
				</label>
				<input type="text" class="form-control" id="restname" name="restname" value="<?php echo $name ?>" >
			</div>
			<div class="form-group">
				 
				<label for="RestLocInput">
					location
				</label>
				<input type="text" class="form-control" id="restloc" name="restloc" value="<?php echo $location ?>" >
			</div>
			<div class="form-group">
				 
				<label for="RestPhoneInput">
					phone
				</label>
				<input type="text"  class="form-control" id="restphone" name="restphone" value="<?php echo $phone ?>" >
			</div>
			<input type="hidden" name="modify_id" value="<?php echo $id ?>" />
			<button type="submit" class="btn btn-default" name="submit" value="restaurant_modify">
				Submit
			</button>
		</form>
			</div>
			<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
		</div>		
  </body>
</html>