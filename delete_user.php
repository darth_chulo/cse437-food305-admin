<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//Initialize Session variable if it hasn't been done already.
	if(!isset($_SESSION))
	{
		session_start();
		
	}
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	
	//If the authentication and username variables have not be setup
	//kick the user back to the login page
	if( !($_SESSION['authenticated'] && !empty($_SESSION['username'])) )
	{
		echo "user is not authenticated";
		//header("Location: http://$host$uri/");
	}

		if(!empty($_GET['id']))
		{

			$m = new MongoClient();

			// select a database
			$db = $m->food305_db;
			
			$collection = $db->user;
					
			$id = array('_id' => new MongoId($_GET['id']));
			
			$item = $collection->findOne($id);
	
			$document = array("justOne" => true);
			
			if(!empty($item))
			{
				$isdeleted = $collection->remove($id,$document);
			
				if(!$isdeleted)
				{
					echo "<br>There has been an error your item was not deleted!";
					echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
				}
				else
				{
					header("Location: admin_panel.php");			
				}
			}
		}
		
?>