<?php
        //If the session has not been initialized, fire it up.
        if(!isset($_SESSION))
        {
                session_start();
                
        }
        //if the authenticated & username session variables are not empty
        //and the user is authenticated, send them home.
        if(!empty($_SESSION['authenticated']) && !empty($_SESSION['username']) && $_SESSION['authenticated'])
        {
                $host  = $_SERVER['HTTP_HOST'];
                $uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                header("Location: admin_panel.php");
        }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Administrative Login</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
<body>
<div class="navigation">
        </div>          <div class="normal">
                        <form method="post" action="login.php">
                                <p id="info">Please enter username and password.</p>
                                <div class="text-area">
                                        <span class="icon"></span>
                                        <input class="input" type=text name=username placeholder="Username"></div>
																				<input class="input" type=text name=password placeholder="Password"></div>
                                <div class="control-area">
                                        <input class="btn-normal" value="login" type=submit name=submit>
                                </div>
                        </form>
                </div>
        </body>
</html>
