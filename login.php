<!DOCTYPE html>
<head><title>Food305 Administrative Processing</title></head>
<body>
	<?php
		//setup the host locations and the relative urls
		$host  = $_SERVER['HTTP_HOST'];
		$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		
		//if the session variable is not initialized, fire it up.
		if (!isset($_SESSION))
		{
			session_start();
		
			if(isset($_POST['username']))
			{
				//Get the username, loop through credentials and
				//give the user access if they are in the list.
				$current_username = htmlentities($_POST['username']);
				$valid_user = FALSE;
				if($current_username == "food305admin")
				{
					$valid_user = TRUE;
					$_SESSION['authenticated'] = TRUE;		
				}				
				//Once we have a valid user, set up the users environment
				//and send them home.
				if($valid_user)
				{
					$_SESSION['username'] = $current_username; 
					header("Location: http://$host$uri/user_home.php");
					header("Location: admin_panel.php");

				}
            	else
				{	//a quick error message
					echo'<p><strong> Invalid Username!</strong></p><br>';
					echo'<form action="index.php">
                    			     <input type="submit" value="Please try again"></form>';
	    		}
			}			
			else
			{	//if they are authenticated, send them home.
				header("Location: admin_panel.php");
			}
		}
	?>
</body>
</html>
