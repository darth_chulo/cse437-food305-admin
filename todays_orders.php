<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Administrative Panel</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

<h1 align="center">
	Today's Orders:
</h1>

<div class="container-fluid">
<div class="col-md-4">
</div>
<div class="col-md-4" style="text-align: center;">
<a href="" class="btn btn-primary btn-lg" type="button">Print</a>
<a href="admin_panel.php" class="btn btn-primary btn-lg" type="button">Close</a>

</div>
<div class="col-md-4">
</div>

	<div class="row">
		<div class="col-md-12">

<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>
									Order ID
								</th>
								<th>
									Restaurant
								</th>
								<th>
									Meal
								</th>
								<th>
									Price
								</th>
								<th>
									Delivery Time
								</th>
								<th>
									User
								</th>
								<th>
									Comment
								</th>
							</tr>
						</thead>
<tbody>
							<?php
								$m = new MongoClient();
			
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->order;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								$class_type = array('active', 'success', 'warning', 'danger');
								
								$price_type = array('$5.00', '$3.75', '$8.99', '$14.56','$25.00', '$13.75', '$28.99', '$34.56');
								
								$i = 1;
								//Order ID Meal Delivery Time User Comment
								
								foreach ($cursor as $document)
								{
									$current_meal = $db->meal->findOne(array('_id' => new MongoId($document['mealid'])));
		
									$current_user = $db->user->findOne(array('_id' => new MongoId($document['userid'])));
									
									$current_restaurant = $db->restaurant->findOne(array('_id' => new MongoId($current_meal['restaurant_id'])));
									
									
									$item_time = date_parse($document['deliverytime']);
									$today = getdate();
									
									if ($item_time[day] == $today['mday'])
									{
										echo '<tr class=' . $class_type[ ($i++ % 3) + 1] . '>'; // "active">';
										
										echo '<td>';
										
										echo '<a class="glyphicon glyphicon-remove" href="delete_order.php?id=' .
											 $document['_id'] . '"></a>';
										
										echo '<a href="edit_order.php?id=' . $document['_id'] . '">' . $document['_id'] . '</a></td>';
										echo '<td>' . $current_restaurant['name']   . '</td>';
										echo '<td>' . $current_meal['name']   . '</td>';
										echo '<td>' . $price_type[($i % 8)] . '</td>';
										echo '<td>' . $document['deliverytime'] . '</td>';
										echo '<td>' . $current_user['email'] . '</td>';
										echo '<td>' . $document['comment'] . '</td></tr>';
										}
								}

							?>
						</tbody>
</table>

</div>
	</div>
</div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>