<?php
                
        if(!isset($_SESSION))
        {
                session_start();        
        }
        
        $host  = $_SERVER['HTTP_HOST'];
    $uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        
        /*Taken from http://7php.com/php-5-3-how-to-completely-destroy-session-variables-in-php/ */
        
        //Blow everything related to the session away.
        
        //remove PHPSESSID from browser
        if ( isset( $_COOKIE[session_name()] ) )
        setcookie( session_name(), “”, time()-3600, “/” );
        //clear session from globals
        $_SESSION = array();
        //clear session from disk
        session_destroy();

        //redirecto to login page
        header("Location: index.php");
?>

