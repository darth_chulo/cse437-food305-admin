<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Administrative Request</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//Initialize Session variable if it hasn't been done already.
	if(!isset($_SESSION))
	{
		session_start();
		
	}
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	
	//If the authentication and username variables have not be setup
	//kick the user back to the login page
	if( !($_SESSION['authenticated'] && !empty($_SESSION['username'])) )
	{
		//echo "user is not authenticated";
		header("Location: index.php");
	}
	
//	echo "<br>Print Post: ";
//	print_r($_POST);
	
//	echo "<br>Print Session: ";
//	print_r($_SESSION);
	
	
	echo '<br><a href="logout.php">Click Here to Logout</a><br>';
	echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
	
	//Setup Mongo Items
	
	// connect
	$m = new MongoClient();

	// select a database
	$db = $m->food305_db;
	
	switch ($_POST['submit'])
	{
		case 'restaurant':
	
			$isadded = false;
			
			if ( !(empty($_POST['restname']) or empty($_POST['restloc']) or empty($_POST['restphone']) ))
			{
					$collection = $db->restaurant;
					$document = array( "name" => $_POST['restname'], "location" => $_POST['restloc'],
										 "phone" => $_POST['restphone'] );
					$isadded = $collection->insert($document);
				
					if(!$isadded)
					{
						echo "<br>There has been an error your item was not updated!";
						echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
					}
					else
					{
						header("Location: admin_panel.php");			
		
					}
			}
			else
			{
						echo "<br>Cut the crap and fix your input ADMIN FIll out all the required sections!";
						echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
			}

			break;
		
		case 'restaurant_modify':
			
			$collection = $db->restaurant;

			
			$id = array('_id' => new MongoId($_POST['modify_id']));
			
			$document = array("name" => $_POST['restname'], "location" => $_POST['restloc'],
							"phone" => $_POST['restphone'] );

			
			$item = $collection->findOne($id);
			
			if(!empty($item))
			{
				$isupdated = $collection->update($id,$document);
			
				if(!$isupdated)
				{
					echo "<br>There has been an error your item was not updated!";
					echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
				}
				else
				{
					header("Location: admin_panel.php");			
				}
			}
			break;
		
		case 'meal':
		

			$isadded = false;
			$prices = explode(",",$_POST['mealprices']);
			$levels = explode(",",$_POST['meallevels']);
			
			$error_check = (bool)((sizeof($prices) != 3) or
														(sizeof($levels) != 3) or
														($_POST['restaurant_id']=='Choose one'));
			if($error_check) 
			{
				if(sizeof($prices) != 3)
				{
					echo "<br>There has been an error with prices, need exactly 3 prices for this item";
				}
				if(sizeof($levels) != 3)
				{
					echo "<br>There has been an error with levels, need exactly 3 levels for this item";
				}
				if($_POST['restaurant_id']='Choose one')
				{
					echo "<br>There has been an error your restaurant, please select a restaurant";
				}
			}
			else
			{
				$collection = $db->meal;
				$document = array( "restaurant_id" => $_POST['restaurant_id'], "name" => $_POST['mealname'],
													"prices" => $_POST['mealprices'],"levels" => $_POST['meallevels'],
													"image" => $_POST['mealimage'] );

				$isadded = $collection->insert($document);	
			}
			
			if(!$isadded)
			{
				echo "<br>There has been an error your item was not updated!";
				echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
			}
			else
			{

				$collection = $db->restaurant;
				$restaurant_id = new MongoID($document['restaurant_id']);
				$meal_id = $document['_id'];
				$isupdated = $collection->update(array("_id"=>$restaurant_id),
																 array('$addToSet' => array("meals" => $meal_id)));
				
				if($isupdated)
				{
					header("Location: admin_panel.php");			
	
				}
				else
				{
					echo "Something went wrong, better check your insertions!";
				}
				
			}
			break;

		case 'meal_modify':
					
					$collection = $db->meal;
		
					$id = array('_id' => new MongoId($_POST['modify_id']));
					
					$document = array( "restaurant_id" => $_POST['restaurant_id'], "name" => $_POST['mealname'],
													"prices" => $_POST['mealprices'],"levels" => $_POST['meallevels']);
													//"image" => $_POST['mealimage'] );
				
					
					$item = $collection->findOne($id);
					
					if(!empty($item))
					{
						$isupdated = $collection->update($id,$document);
					
						if(!$isupdated)
						{
							echo "<br>There has been an error your item was not updated!";
							echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
						}
						else
						{
							header("Location: admin_panel.php");			
						}
					}
					break;
				
				case "useradd":
				
					$isadded = false;
					
					$collection = $db->user;
				
					$input_check = (bool) ((empty($_POST['email']) or  empty($_POST['password']) or
							empty($_POST['firstname']) or empty($_POST['lastname'])));
					
					if (!$input_check)
					{
					
						$document = array( "email" => $_POST['email'], "password" => $_POST['password'],
										 "fname" => $_POST['firstname'], "lname" => $_POST['lastname'] );
				
						$isadded = $collection->insert($document);
						
						if(!$isadded)
						{
							echo "<br>There has been an error your item was not updated!";
							echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
						}
						else
						{
							header("Location: admin_panel.php");			
			
						}
					}
					else
					{
						echo "<br>Please enter all the fields to enter user!";
						echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
					}
						
					break;
		
				case 'user_modify':
					
					$collection = $db->user;

					$id = array('_id' => new MongoId($_POST['modify_id']));
					
					$document = array( "email" => $_POST['email'], "password" => $_POST['password'],
										 "fname" => $_POST['firstname'], "lname" => $_POST['lastname'] );
				
					$item = $collection->findOne($id);
					
					if(!empty($item))
					{
						$isupdated = $collection->update($id,$document);
					
						if(!$isupdated)
						{
							echo "<br>There has been an error your item was not updated!";
							echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
						}
						else
						{
							header("Location: admin_panel.php");			
						}
					}
					break;
	
		case "order":
				
			$isadded = false;
					
			$collection = $db->order;
				
			$input_check = (bool) (($_POST['mealid'] == 'Choose one') or
														($_POST['userid'] == 'Choose one') );
			
			if (!$input_check)
			{


				$document = array("mealid" => $_POST['mealid'], "userid" => $_POST['userid'],
								 "deliverytime" => $_POST['deliverytime'], "comment" => $_POST['comment']);
		
				$isadded = $collection->insert($document);
				
				if(!$isadded)
				{
					echo "<br>There has been an error your item was not updated!";
					echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
				}
				else
				{

					$collection = $db->meal;
					$meal_id = new MongoID($document['mealid']);
					$order_id = $document['_id'];
					$isupdated = $collection->update(array("_id"=>$meal_id),
																					 array('$addToSet' => array("orders" => $order_id)));

					header("Location: admin_panel.php");			
	
				}
			}
			else
			{
				echo "<br>Please enter all the fields to enter user!";
				echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
			}
					
			
			break;
		
		case "order_modify":
		
				$collection = $db->order;
		
				$id = array('_id' => new MongoId($_POST['modify_id']));
					
				$document = array("mealid" => $_POST['mealid'], "userid" => $_POST['userid'],
								 "deliverytime" => $_POST['deliverytime'], "comment" => $_POST['comment']);
	
					$item = $collection->findOne($id);
					
					if(!empty($item))
					{
						$isupdated = $collection->update($id,$document);
					
						if(!$isupdated)
						{
							echo "<br>There has been an error your item was not updated!";
							echo '<br><a href="admin_panel.php">Click Here to go back to the Admin Panel</a><br>';
						}
						else
						{
							header("Location: admin_panel.php");			
						}
					}
		break;
			
			
	}
?>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>