<?php

	//echo "Var Dump";
	//var_dump($_POST);
	
	//Initialize Session variable if it hasn't been done already.
	if(!isset($_SESSION))
	{
		session_start();
		
	}
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	
	//If the authentication and username variables have not be setup
	//kick the user back to the login page
	if( !($_SESSION['authenticated'] && !empty($_SESSION['username'])) )
	{
		echo "user is not authenticated";
		//header("Location: http://$host$uri/");
	}
	/*
	echo "<br>Print Post: ";
	print_r($_POST);
	
	echo "<br>Print Session: ";
	print_r($_SESSION);
	*/
	
	echo '<br><a href="logout.php">Click Here to Logout</a><br>';			
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Administrative Panel</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						Restaurant
					</h2>
					<p>
						Restaurant Listings
					</p>
					<table class="table">
						<thead>
							<tr>
						<!--		<th>
									Restaurant ID
								</th>
						-->
								<th>
									Name
								</th>
								<th>
									Location
								</th>
								<th>
									Phone
								</th>
						<!--		<th>
									Meals
								</th>
						-->
							</tr>
						</thead>
							<tbody>
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->restaurant;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								$class_type = array('active', 'success', 'warning', 'danger');
								//echo(mt_rand(10,100));
								// iterate through the results
								$i = 1;
								foreach ($cursor as $document)
								{
									//echo '<tr class=' . $class_type[mt_rand(1,4)] . '>'; // "active">';
									echo '<tr class=' . $class_type[ ($i++ % 3) + 1] . '>'; // "active">';
									
									echo '<td>';
									
									echo '<a class="glyphicon glyphicon-remove" href="delete_restaurant.php?id=' .
										 $document['_id'] . '"></a>';
									
						//			echo '<td>' . $document['_id'] . '</td>';
									//echo '<td>' . '<a href="editrestaurant_id.php?id=' . $document['_id'] . '">' . $document['name'] . '</a></td>';
									echo '<a href="edit_restaurant.php?id=' . $document['_id'] . '">' . $document['name'] . '</a></td>';
									echo '<td>' . $document['location'] . '</td>';
									echo '<td>' . $document['phone'] . '</td></tr>';
									//echo print_r($document);
									
								}
								
						//		echo '</tr>';
							?>
						</tbody>
					</table>
					<form role="form" action="process_request.php" method="post">
						<div class="form-group">
							 
							<label for="RestNameInput">
								Restaurant Name
							</label>
							<input type="text" class="form-control" id="restname" name="restname">
						</div>
						<div class="form-group">
							 
							<label for="RestLocInput">
								location
							</label>
							<input type="text" class="form-control" id="restloc" name="restloc">
						</div>
						<div class="form-group">
							 
							<label for="RestPhoneInput">
								phone
							</label>
							<input type="text"  class="form-control" id="restphone" name="restphone">
						</div>
					<!--
						<div class="form-group">
					
							<label for="RestMealsInput">
								meals
							</label>
							<input type="text" class="form-control" id="restmeals" name="restmeals">
						</div>
					-->	
						<button type="submit" class="btn btn-default" name="submit" value="restaurant">
							Submit
						</button>
					</form>
				</div>
				<div class="col-md-5">
					<h2>
						Meals
					</h2>
					<p>
						List of Meals
					</p>
					<table class="table">
						<thead>
							<tr>
						<!--		<th>
									meal-id
								</th>
						-->
								<th>
									Restaurant
								</th>
						
								<th>
									Name
								</th>
								<th>
									Prices
								</th>
								<th>
									Levels
								</th>
								<th>
									Image
								</th>
								<th>
									Orders
								</th>
						<!--		<th>
									misc
								</th>
						-->
							</tr>
						</thead>
						<tbody>
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->meal;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								$class_type = array('active', 'success', 'warning', 'danger');
								
								$i = 1;
								
								foreach ($cursor as $document)
								{
									$restaurant = $db->restaurant->findOne(array('_id' => new MongoId($document['restaurant_id'])));

									$rest_name = $db->restaurant->collection->findOne(array('_id' => new MongoId($document['restaurant_id'])));
									
									$order_count = $db->order->count(array("mealid" => ((string) $document['_id'])));
									
									echo '<tr class=' . $class_type[ ($i++ % 3) + 1] . '>'; // "active">';
									
									echo '<td>';
									
									echo '<a class="glyphicon glyphicon-remove" href="delete_meal.php?id=' .
										 $document['_id'] . '"></a>';
									
									echo '<a href="edit_meal.php?id=' . $document['_id'] . '">' . $restaurant['name']  . '</a></td>';
									echo '<td>' . $document['name'] . '</td>';
									echo '<td>' . $document['prices'] . '</td>';
									echo '<td>' . $document['levels'] . '</td>';
									echo '<td>' . ' ' . '</td>';
									echo '<td>' . ($order_count ?  $order_count : ' ') . '</td></tr>';
								}
							?>
						</tbody>
					</table>
					<form role="form" action="process_request.php" method="post">
						<div class="form-group">
						<!--	 
							<label for="mealInputID">
								Meal ID
							</label>
							<input type="text" class="form-control" id="mealID" name="mealid">
						</div>
						<div class="form-group">
							 
							<label for="mealResID">
								Restaurant ID
							</label>
							<input type="text" class="form-control" id="mealResID" name="mealresid">
						</div>
					-->
						<div class="form-group">
							 
							<label for="mealResID">
								Restaurant ID
							</label>
						
						<select name="restaurant_id" id="restaurant_id">
							<option selected="selected">Choose one</option>
								
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->restaurant;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								foreach($cursor as $document)
								{ 
									echo '<option value=' . $document['_id'] . '>' .
										  $document['name'] . '</option>';
								}
							?>
							</select> 
						</div>
						<div class="form-group">
							 
							<label for="mealName">
								Meal Name
							</label>
							<input type="text" class="form-control" id="mealName" name="mealname">
						</div>
						<div class="form-group">
							 
							<label for="mealPrices">
								Meal Prices
							</label>
							<input type="text" class="form-control" id="mealPrice" name="mealprices">
						</div>
						<div class="form-group">
							 
							<label for="mealLevel">
								Meal Level
							</label>
							<input type="text" class="form-control" id="meallevel" name="meallevels">
						</div>
						<div class="form-group">	 
							<label for="MealImage">
								Meal Image
							</label>
							<input type="file" id="mealimage" name="mealimage">
							<p class="help-block">
								Upload image here.
							</p>
						</div>
						<!--
						<div class="checkbox">
							 
							<label>
								<input type="checkbox"> Some box to check
							</label>
						</div>
						<div class="form-group">
							 
							<label for="mealOrders">
								Meal Orders
							</label>
							<input type="text" class="form-control" id="mealorder" name="mealorder">
						</div>
						<div class="form-group">
							 
							<label for="mealMisc">
								Meal Misc
							</label>
							<input type="text" class="form-control" id="mealmisc" name="mealmisc">
						</div>
						-->
						<button type="submit" class="btn btn-default" name="submit" value="meal">
							Submit
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						Order
					</h2>
					<p>
						Order Listings
					</p>
					<p>
						<a class="btn" href="todays_orders.php">View details »</a>
					</p>
					<table class="table">
						<thead>
							<tr>
								<th>
									Order ID
								</th>
								<th>
									Meal
								</th>
								<th>
									Delivery Time
								</th>
								<th>
									User
								</th>
								<th>
									Comment
								</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$m = new MongoClient();
			
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->order;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								$class_type = array('active', 'success', 'warning', 'danger');
								
								$i = 1;
								//Order ID Meal Delivery Time User Comment
								
								foreach ($cursor as $document)
								{
									$current_meal = $db->meal->findOne(array('_id' => new MongoId($document['mealid'])));
		
									$current_user = $db->user->findOne(array('_id' => new MongoId($document['userid'])));
									
									echo '<tr class=' . $class_type[ ($i++ % 3) + 1] . '>'; // "active">';
									
									echo '<td>';
									
									echo '<a class="glyphicon glyphicon-remove" href="delete_order.php?id=' .
										 $document['_id'] . '"></a>';
									
									echo '<a href="edit_order.php?id=' . $document['_id'] . '">' . $document['_id'] . '</a></td>';
									echo '<td>' . $current_meal['name']   . '</td>';
									echo '<td>' . $document['deliverytime'] . '</td>';
									echo '<td>' . $current_user['email'] . '</td>';
									echo '<td>' . $document['comment'] . '</td></tr>';
									
								}
								
							?>
						</tbody>
					</table>
					<form role="form" action="process_request.php" method="post">
						<!--
						<div class="form-group">
							 
							<label for="OrderID">
								Order ID
							</label>
							<input type="text" class="form-control" id="orderid" name="orderid">
						</div>
					-->
						<div class="form-group">
							 
							<label for="Meal">
								Meal
							</label>
							
							<select name="mealid" id="mealid">
							<option selected="selected">Choose one</option>
								
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->meal;
										
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								foreach($cursor as $document)
								{
									
									$restaurant = $db->restaurant->findOne(array('_id' => new MongoId($document['restaurant_id'])));
								
									echo '<option value=' . $document['_id'] . '>' .
										  $document['name'] . '  -  ' . $restaurant['name']  . '</option>';
								}
							?>
							</select>
						<!--	<input type="text" class="form-control" id="mealid" name="mealid">
						-->
						</div>
						<div class="form-group">
							 
							<label for="DeliveryTime">
								Delivery Time
							</label>
							<input type="datetime-local" class="form-control" id="deliverytime" name="deliverytime">
							<!--<input type="text" class="form-control" id="restphone" name="restphone">-->
						</div>
						<div class="form-group">
					
							<label for="UserID">
								User
							</label>
							
							<select name="userid" id="userid">
							<option selected="selected">Choose one</option>
								
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->user;
										
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('email' => 1));
								
								foreach($cursor as $document)
								{
									
									//$restaurant = $db->restaurant->findOne(array('_id' => new MongoId($document['restaurant_id'])));
								
									echo '<option value=' . $document['_id'] . '>' .
										  $document['email'] . ' : ' . $document['fname'] . ' ' . $document['lname'] . '</option>';
								}
							?>
							</select>
						</div>
						<div class="form-group">
					
							<label for="OrderComment">
								comment
							</label>
							<input type="text" class="form-control" id="comment" name="comment">
						</div>
						<button type="submit" class="btn btn-default" name="submit" value="order">
							Submit
						</button>
					</form>
				</div>
				<div class="col-md-5">
					<h2>
						Users
					</h2>
					<p>
						List of Users
					</p>
					<table class="table">
						<thead>
							<tr>
								<th>
									email
								</th>
								<th>
									password
								</th>
								<th>
									First Name
								</th>
								<th>
									Last Name
								</th>
								<th>
									user-id
								</th>
							</tr>
						</thead>
						<tbody>
						<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->user;
									
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('email' => 1));
								
								$class_type = array('active', 'success', 'warning', 'danger');
								
								$i = 1;
								
								foreach ($cursor as $document)
								{									
									echo '<tr class=' . $class_type[ ($i++ % 3) + 1] . '>'; // "active">';	
									echo '<td>';
									
									echo '<a class="glyphicon glyphicon-remove" href="delete_user.php?id=' .
										 $document['_id'] . '"></a>';
									
									echo '<a href="edit_user.php?id=' . $document['_id'] . '">' . $document['email'] . '</a></td>';
									echo '<td>' . $document['password'] . '</td>';
									echo '<td>' . $document['fname'] . '</td>';
									echo '<td>' . $document['lname'] . '</td>';
									echo '<td>' . $document['_id'] . '</td></tr>';
								}
								
							?>
						</tbody>
					</table>
					<form role="form" action="process_request.php" method="post">
						
						<!--<div class="form-group">
							 
							<label for="UserID">
								User-ID
							</label>
							<input type="text" class="form-control" id="userid" name="userid">
						</div>-->
						<div class="form-group">
							 
							<label for="UserEmail">
								Email
							</label>
							<input type="email" class="form-control" id="useremail" name="email">
						</div>
						<div class="form-group">
							 
							<label for="UserPassword">
								Password
							</label>
							<input type="text" class="form-control" id="userpassword" name="password">
						</div>
						<div class="form-group">
							 
							<label for="UserFName">
								First Name
							</label>
							<input type="text" class="form-control" id="userfname" name="firstname">
						</div>
						<div class="form-group">
							 
							<label for="UserLName">
								Last Name
							</label>
							<input type="text" class="form-control" id="userlname" name="lastname">
						</div>
						<button type="submit" class="btn btn-default" id="usersubmit" name="submit" value="useradd">
							Submit
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
    

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>