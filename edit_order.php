<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//Initialize Session variable if it hasn't been done already.
	if(!isset($_SESSION))
	{
		session_start();
		
	}
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	
	//If the authentication and username variables have not be setup
	//kick the user back to the login page
	if( !($_SESSION['authenticated'] && !empty($_SESSION['username'])) )
	{
		echo "user is not authenticated";
		header("Location: index.php");
	}
		$item = "";
		if(!empty($_GET['id']))
		{

			$id = $_GET['id'];
			
			$m = new MongoClient();

			// select a database
			$db = $m->food305_db;			
			$collection = $db->order;
			$item = $collection->findOne(array('_id' => new MongoId($_GET['id'])));
		}
		
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food305 Admin Meal Modify</title>

    <meta name="description" content="Source code generated for food305 using layoutit.com">
    <meta name="author" content="Steven Harris">

	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
  </head>
	<body>
		<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">
					<form role="form" action="process_request.php" method="post">
						<div class="form-group">
							 
							<label for="Meal">
								Meal
							</label>
							
							<select name="mealid" id="mealid">
							<option selected="selected">Choose one</option>
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->meal;
										
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('name' => 1));
								
								foreach($cursor as $document)
								{
									
									$restaurant = $db->restaurant->findOne(array('_id' => new MongoId($document['restaurant_id'])));
									$selected = ($document['_id'] == $item['mealid'] ? ' selected="selected" ' : " ");
									echo '<option' . $selected .'value=' . $document['_id'] . '>' .
										  $document['name'] . '  -  ' . $restaurant['name']  . '</option>';
								}
							?>
							</select>
						</div>
						<div class="form-group">
							 
							<label for="DeliveryTime">
								Delivery Time
							</label>
							<input type="datetime-local" class="form-control" id="deliverytime" name="deliverytime" value="<?php echo $item['deliverytime']; ?>" >
						</div>
						<div class="form-group">
					
							<label for="UserID">
								User
							</label>
							
							<select name="userid" id="userid">
							<option selected="selected">Choose one</option>
							<?php
								$m = new MongoClient();
			
								// select a database
								$db = $m->food305_db;
								
								$collection = $db->user;
										
								// find everything in the collection
								$cursor = $collection->find();
								
								$cursor->sort(array('email' => 1));
								
								foreach($cursor as $document)
								{				
									$selected = ($document['_id'] == $item['userid'] ? ' selected="selected" ' : " ");
									echo '<option' . $selected . 'value=' . $document['_id'] . '>' .
										  $document['email'] . ' : ' . $document['fname'] . ' ' . $document['lname'] . '</option>';
								}
							?>
							
							</select>
						</div>
						<div class="form-group">
					
							<label for="OrderComment">
								comment
							</label>
							<input type="text" class="form-control" id="comment" name="comment" value="<?php echo $item['comment']; ?>">
						</div>
						<input type="hidden" name="modify_id" value="<?php echo $id ?>" />
						<button type="submit" class="btn btn-default" name="submit" value="order_modify">
							Submit
						</button>
					</form>
			</div>
			<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
		</div>		
  </body>
</html>